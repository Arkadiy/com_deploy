<?php
defined( '_JEXEC' ) or die; // No direct access
/**
 * Component deploy
 * @author Arkadiy
 */

$controller = JControllerLegacy::getInstance( 'deploy' );
$controller->execute( JFactory::getApplication()->input->get( 'task' ) );
$controller->redirect();