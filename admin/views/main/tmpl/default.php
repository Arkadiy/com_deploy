<?php
/** @var $this DeployViewMain */
defined( '_JEXEC' ) or die; // No direct access
?>
<div class="row item-page">
    <div class="span4"></div>
    <div class="span4">
        <h2>Настройки</h2>
        <ul>
            <li>Заполнить в настройках компонента все поля.</li>
            <li>Зайти в настройки репозитроия на bitbucket.org</li>
            <li>Зайти в Integrations -> Webhooks</li>
            <li>Нажать кнопку "Add webhook"</li>
            <li>
                В появившемся окне ввести заголовок (любой) урл http://my_site.ru/index.php?option=com_deploy&task=deploy
                радио "Triggers" установить в "Repository push"
            </li>
            <li>Сохранить запись.</li>
        </ul>
        <h2>Использование</h2>
        <p>
            Компонент заливает изменения из указанной в настройках ветки репозитория на сайт. Не предусмотрено разное
            расположение файлов на сайте и в репозитории, т.е. сайт в репозитории должен располагаться в корне репозитория.
        </p>



        <h2>Settings</h2>
        <ul>
            <li>Fill in all the settings of the component fields.</li>
            <li>Go to settings on repositroy bitbucket.org</li>
            <li>Go to Integrations -> Webhooks</li>
            <li>Click "Add webhook"</li>
            <li>
                In the window that appears, enter a title (any) url
                http://my_site.ru/index.php?option=com_deploy&task=deploy
                radio "Triggers" set in the "Repository push"
            </li>
            <li>Save.</li>
        </ul>
        <h2>Use</h2>
        <p>
            Component fills changes from selected branches of the repository on the website.
            Cannot accommodate the different location of the files on the website and in the repository,
            a site in the repository should be located in the root of the repository.
        </p>
    </div>
    <div class="span4"></div>

</div>