<?php
defined( '_JEXEC' ) or die; // No direct access

/**
 * View for  current element
 * @author Arkadiy
 */
class DeployViewMain extends JViewLegacy
{
	/**
	 * Method of display current template
	 * @param type $tpl
	 */
	public function display( $tpl = null )
	{
		JToolbarHelper::title(JText::_('COM_DEPLOY'), 'bookmark');
		$this->addToolbar();
		parent::display( $tpl );
	}

	protected function addToolbar()
	{
		$user = JFactory::getUser();
		if ($user->authorise('core.admin', 'com_deploy') || $user->authorise('core.options', 'com_deploy'))
		{
			JToolbarHelper::preferences('com_deploy');
		}
	}
}