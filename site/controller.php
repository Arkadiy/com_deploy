<?php
defined( '_JEXEC' ) or die; // No direct access

/**
 * Default Controller
 * @author Arkadiy
 */
class DeployController extends JControllerLegacy
{
	function deploy()
	{
		$app = JFactory::getApplication();
		$post_data = json_decode(file_get_contents('php://input'), TRUE);
		if(is_array($post_data) && count($post_data))
		{
			$DeploySiteHelper = new DeploySiteHelper;
			$DeploySiteHelper->deploy($post_data);
		}
		$app->close();
	}
}