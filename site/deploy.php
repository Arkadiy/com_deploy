<?php
defined( '_JEXEC' ) or die; // No direct access
/**
 * Component deploy
 * @author Arkadiy
 */
require_once JPATH_COMPONENT.'/helpers/deploy.php';
$controller = JControllerLegacy::getInstance( 'deploy' );
$controller->execute( JFactory::getApplication()->input->get( 'task' ) );
$controller->redirect();