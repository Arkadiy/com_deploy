<?php

// No direct access
defined( '_JEXEC' ) or die;

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

/**
 * Component helper
 * @author Arkadiy
 */
class DeploySiteHelper
{
	var
			$git_slug,
			$branch,
			$bitbucket_username,
			$bitbucket_password,
			$log,
			$logger
	;

	function __construct()
	{
		$config = JComponentHelper::getParams('com_deploy');
		$this->git_slug = $config->get('git_slug', '');
		$this->branch = $config->get('branch', '');
		$this->bitbucket_username = $config->get('bitbucket_username', '');
		$this->bitbucket_password = $config->get('bitbucket_password', '');
		$this->log = $config->get('log', 1);
		if($this->log){
			$options = array('text_file' => 'com_deploy.php');
			$this->logger = new JLogLoggerW3c($options);
		}
	}

	function logIt($text,$save=true)
	{
		if(!$this->log)
		{
			return '';
		}
		$msg = date("d.m.Y, H:i:s",time()) .': '.$text."\n";
		if(!$save)
		{
			return $msg;
		}
		$entry = new JLogEntry($msg);
		$this->logger->addEntry( $entry );
		return '';
	}

	function logMsg($text) {
		if(!$this->log){
			return;
		}
		$entry = new JLogEntry($text);
		$this->logger->addEntry( $entry );
	}

	function deploy($data)
	{
		if(!isset($data['push']) || !isset($data['push']['changes']) || !count($data['push']['changes'])){
			$this->logIt('No repository changes');
			return;
		}

		$slug = $data['repository']['name'];
		$repoUri = $data['repository']['full_name'];

		if($slug != $this->git_slug)
		{
			$this->logIt('Slug '.$slug.' != '.$this->git_slug);
			return;
		}

		$commit_hashes = array();

		foreach($data['push']['changes'] as $commit)
		{
			if($commit["old"]["name"] == $this->branch)
			{
				$commit_hashes[$commit["old"]["target"]["hash"]] = $commit["old"]["target"]["hash"];
			}
			if($commit["new"]["name"] == $this->branch)
			{
				$commit_hashes[$commit["new"]["target"]["hash"]] = $commit["new"]["target"]["hash"];
			}
		}

		$this->logIt('Commit hashes '.json_encode($commit_hashes));

		if(!count($commit_hashes))
		{
			return;
		}

		foreach ($commit_hashes as $hash)
		{
			$url = 'https://bitbucket.org/api/1.0/repositories/'.$repoUri.'/changesets/'.$hash;
			$data = $this->getData($url);

			$this->logIt('Commit url '.$url);
			$this->logIt('Data '.$data);

			$commit_data = json_decode($data, TRUE);

			if(is_array($commit_data) && isset($commit_data['files']) && count($commit_data['files']))
			{
				$node = $commit_data['node'];
				foreach ($commit_data['files'] as $file)
				{

					$path = $file['file'];
					$filePath = JPATH_ROOT.'/'.$path;
					if ($file['type']=="removed")
					{
						if(JFile::delete($filePath))
						{
							$this->logIt('Removed '.$filePath);
						}
						else{
							$this->logIt('ERROR Remove '.$filePath);
						}
					}
					else
					{
						$dirname = dirname($path);

						if(!JFolder::exists(JPATH_ROOT.'/'.$dirname))
						{
							if(JFolder::create(JPATH_ROOT.'/'.$dirname, 0755))
							{
								$this->logIt('Created new directory '.$dirname);
							}
							else
							{
								$this->logIt('Error: failed to create new directory '.$dirname);
							}
						}

						$url = "https://api.bitbucket.org/1.0/repositories/".$repoUri."/raw/".$node."/".$path;
						$data = $this->getData($url);

						if($data === false){
							$this->logIt('Bad file data: '.$filePath);
						}
						else if(JFile::write($filePath, $data) === false)
						{
							$this->logIt('Not write file: '.$filePath);
						}
						else
						{
							chmod($filePath, 0644);
							$this->logIt('Uploaded: '.$filePath);
						}
					}
				}
			}
		}
	}

	private function getData($url){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, $this->bitbucket_username.":".$this->bitbucket_password);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		$data = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$this->logIt('HTTP code: '.$code);
		if($code != '200')
		{
			return false;
		}
		return $data;
	}
}